// alert('hi')

// Common examples of arrays

let grades = [98.5, 94.3, 89.2, 90.1];

let marvelHeroes = ["Iron Man", "Captain America", "Thor", "Hulk", "Black Widow", "Hawkeye", "Shang Chi", "Spiderman"];

// Possible use of an array but not recommended
let mixedArray = [12, "Asus", null, undefined, {}];

// Alternative way to write an array
let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake react"
];

// Arrays and Indexes
	// []- array literals
	// index starts with 0


	// Reassign values in an array
	console.log(myTasks);
	myTasks[0] = "sleep for 8 hours";
	console.log(myTasks);

	// Accessing from an array
	console.log(grades[2]);
		// result: 89.2
	console.log(marvelHeroes[6]);
		// result: Shang Chi
	console.log(myTasks[20]);
		// result: undefined

	// Getting the length of an array
		// arrays have access to ".length" property to get the number of elements present in an array

	console.log(marvelHeroes.length);
		// result: 8

	// This is useful for executing code that depends on the content of our array
	if(marvelHeroes.length > 5){
		console.log('We have too many heroes, please contact Thanos')
	};

	// Accessing the last element of an array
		// since the first element of an array starts with 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element

	let lastElement = marvelHeroes.length-1
	console.log(marvelHeroes[lastElement])
		// result: Spider Man

	// Array Methods

		// Mutator Methods
			// these are functions that "mutate" or change an array after they're created

		let fruits = ["Apple", "Blueberry", "Orange", "Grapes"]

		// push()
			// adds an element in the end of an array and returns the array's length

		console.log(fruits);
		let fruitsLength = fruits.push("Mango");
		console.log(fruits);

		// Adding multiple elements
		fruits.push("Guava", "Kiwi");
		console.log(fruits);

		// pop()
			// removes the last element in an array and returns the removed element

		let removedFruit = fruits.pop();
		console.log(removedFruit);
		console.log(fruits);

		fruits.pop();
		console.log(fruits);

		// unshift()
			// adds an element/s at the beginning of an array

		fruits.unshift("Guava");
		console.log(fruits);

		fruits.unshift("Kiwi", "Lime");
		console.log(fruits);

		// shift()
			// removing an element at the beginning of an array

		fruits.shift();
		console.log(fruits);

		/*
			note:
			push and unshift- adding elements
			pop and shift- removing element

		*/

		// splice()
			// simultaneously removes elements from a specified index number and adds elements
			/*
				Syntax: arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
			*/

			fruits.splice(1, 2, "Cherry", "Watermelon");
			console.log(fruits);

			// removes from index 3 to the last element
			fruits.splice(3);
			console.log(fruits);

			// removing elements in the middle
			fruits.splice(1, 1);
			console.log(fruits)

			// adding in the middle of an array
			fruits.splice(2, 0, "Cherry", "Buko");
			console.log(fruits);

		// sort()
			// rearranges the array elements in alphanumeric order

			fruits.sort();
			console.log(fruits);

		// reverse()
			// reverses the order of the array elements

			fruits.reverse();
			console.log(fruits);

	// Non- Mutator Methods
		// these functions do not modify or change an array

	let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"]

	// indexOf()
		// returns the index number of the first matching element found in an array
		// Syntax: arrayName.indexOf(searchValue)

	console.log(countries.indexOf("PH"));
		// result: 1

	let indexOfSG = countries.indexOf("SG");
	console.log(indexOfSG);
		// result: 3

	let invalidCountry = countries.indexOf("SK");
	console.log(invalidCountry);
		// result: -1

	let invalidCountry2 = countries.indexOf("JP");
	console.log(invalidCountry2);
		// result: -1

	// console.log(countries);

	// slice()
		// slices elements from an array and returns a new array
		/*
			Syntax:
				arrayName.slice(startingIndex);
				arrayName.slice(startingIndex, endingIndex);
		*/

		// slice off elements from a specified index to the last element

		let slicedArrayA = countries.slice(2);
		console.log(slicedArrayA);
			// result: ['CAN', 'SG', 'TH', 'PH', 'FR', 'DE']
		// console.log(countries);

		// slice off elements from specified index to another index

		let slicedArrayB = countries.slice(2, 4);
		console.log(slicedArrayB);
			// result: ['CAN', 'SG']

		// slice off elements starting from the last element of an array

		let slicedArrayC = countries.slice(-3);
		console.log(slicedArrayC);
			// result: ["PH, "FR", "DE"]

	// toString()
		// returns an array as string separated by commas
		// Syntax: arrayName.toString();

		let stringArray = countries.toString();
		console.log(stringArray);
			// result: US,PH,CAN,SG,TH,PH,FR,DE

		let sentence = ["I", "like", "javascript", ".", "It's", "fun", "!"]

		let sentenceString = sentence.toString();
		console.log(sentenceString);

	// concat()
		// combines two arrays and returns a combined result
		// Syntax: arrayA.concat(arrayB);
				 //arrayA.concat(elementA); 

		let tasksArrayA = ["drink html", "eat javascript"];
		let tasksArrayB = ["inhale css", "breathe sass"];
		let tasksArrayC = ["get git", "be node"];

		let tasks = tasksArrayA.concat(tasksArrayB);
		console.log(tasks);

		// Combining multiple arrays

		let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
		console.log(allTasks);

		// Combining array with elements

		let combinedTasks = tasksArrayA.concat("smell express", "throw react");
		console.log(combinedTasks);

		// console.log(tasksArrayA);

	// join()
		// returns an array as a string separated by a specified separator string

		let members = ["Rose", "Lisa", "Jisoo", "Jennie"];

		let joinedMembers1 = members.join();
		console.log(joinedMembers1);
			// result: comma as separator

		let joinedMembers2 = members.join("");
		console.log(joinedMembers2);
			// result: no space

		let joinedMembers3 = members.join(" ");
		console.log(joinedMembers3);
			// result: space as separator

		let joinedMembers4 = members.join("/");
		console.log(joinedMembers4);
			// result: / as a separator

	// Iteration Methods
		// they're loops designed to perform repetitive tasks on arrays
		// useful for manipulating array data resulting in complex tasks
		// have a function inside of the method


	// forEach()
	
	// an empty array will store the filtered elements in the iteration method. This is to avoid confusion by modifying the original array.
	let filteredTasks = [];
	
	allTasks.forEach(function(task){
		console.log(task);

		if(task.length > 10){

			filteredTasks.push(task);

		};
	});

	console.log(filteredTasks);

	// map()
		// iterates on each element and returns new array with different values
		// unlike forEach method, map method requires the use of return keyword in order to create another array with the performed operation


		let numbers = [1, 2, 3, 4, 5];

		let numberMap = numbers.map(function(number){
			return number * number;
		});

		console.log(numberMap);

	// every()
		// checks if all elements meet a certain condition
		// returns a true value if all elements meet the condition and false if otherwise 

		let allValid = numbers.every(function(number){
			return (number < 3);
		});

		console.log(allValid);
		// result: false

	// some()
		// checks at least one element in the array that meets the condition
		// returns a true value if at least one element meets the condition, false if otherwise

		let someValid = numbers.some(function(number){
			return (number < 2);
		});

		console.log(someValid);
		// result: true

	// filter()
		// returns a new array that contains the elements that meets a certain condition
		// if there is no elements found, it will return an empty array

		let filterValid = numbers.filter(function(number){
			return (number < 3);
		});

		console.log(filterValid);
		// result: [1, 2]

	let colors = ["red", "green", "black", "orange", "yellow"];

	let values = ["red", "black", "yellow"];

	colors = colors.filter(item => values.indexOf(item) === -1)
	console.log(colors);

	/*
		colors = colors.filter(function(item){
			return (values.indexOf(item) === -1)
		})
	*/