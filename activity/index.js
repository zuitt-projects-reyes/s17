/*
	ACTIVITY:

	>> Create an addStudent() function that will accept a name of the student and add it to the student array.

	>> Create a countStudents() function that will print the total number of students in the array.

	>> Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.

	STRETCH GOALS:

	>> Create a findStudent() function that will do the following:
		--Search for a student name when a keyword is given (filter method).
		--If one match is found print the message studentName is an enrollee.
		--If multiple matches are found print the message studentNames are enrollees.
		--If no match is found print the message studentName is not an enrollee.
		--The keyword given should not be case sensitive.

*/

const students = ['bc', 'ab', 'cd'];

const addStudent = name => students.push(name);
const countStudents = () => console.log(students.length);
const printStudents = () => students.sort().forEach(student => console.log(student));
const findStudent = keyword => {
	const matches = students.filter(student => student.includes(keyword));
	if (matches.length === 1) {
		console.log(`${matches[0]} is an enrollee.`);
	} else if (matches.length > 1) {
		console.log(`${matches.join(', ')} are enrollees.`);
	} else {
		console.log(`No student found with the name ${keyword}.`);
	}
}

addStudent('da');
countStudents();
printStudents();
findStudent('a');